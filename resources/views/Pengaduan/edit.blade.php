@extends('layouts.MainD')

@section('title', 'Edit | Pengaduan')

@section('content')
<div class=" ml-[296px] mr-11 mt-[116px] mx-auto font-jakartasans">
    <div class="bg-putih w-full rounded-xl h-full pt-6 pb-16 px-6">
        <div class=" flex">
            <h1 class=" font-bold">Edit Pengaduan</h1>
        </div>
        <div class=" mt-11">
            <form action="{{route('routePN.update', ['id_pengaduan' =>$pengaduans->id])}}" method="POST">
                @csrf
                @method('PATCH')
                <div class=" w-full flex flex-col gap-4">
                    <div class=" flex items-center gap-3">
                        <div class=" w-1/2 flex flex-col gap-2">
                            <label for="isi_laporan" class=" text-xs">Isi laporan</label>
                            <input type="text" placeholder="Isi laporan" id="isi_laporan" value="{{$pengaduans->isi_laporan}}" name="isi_laporan" class=" w-full placeholder:text-xs text-xs focus:placeholder:text-kedua text-utama focus:outline-none border rounded-lg border-kedua p-4" required>
                        </div>
                        <div class=" w-1/2 flex flex-col gap-2 relative">
                            <label for="image" class=" text-xs">Image</label>
                            <input type="file" placeholder="Image" id="image" value="{{$pengaduans->profileImage}}" name="image" class=" w-full placeholder:text-xs text-xs focus:placeholder:text-kedua text-utama focus:outline-none border rounded-lg border-kedua p-[13px]" required>
                            <img class=" w-6 h-6 absolute right-0 top-9 -translate-x-5" src="/img/{{$pengaduans->image}}">
                        </div>
                    </div>
                    <button type="submit" class=" flex items-center justify-center bg-biru text-putih p-4 gap-1 hover:bg-opacity-80 rounded-xl">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m6-6H6" />
                        </svg>
                        <span>Edit</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
