@extends('layouts.MainLR')

@section('title', 'Register')

@section('content')
    <div class=" mx-auto">
        <nav class=" mx-auto">
            <div class=" px-14 py-3 fixed w-full bg-utama">
                <div class=" flex justify-between">
                    <div class=" flex items-center gap-2">
                        <img class=" w-14 h-14" src="{{asset('img/tes.svg')}}">
                        <h1 class=" text-putih font-bold">Pengaduan BS</h1>
                    </div>
                    <div class=" flex items-center gap-4">
                        <a href="{{route('routeLP.landing')}}" class="before:bg-putih  before:border-utama border bg-utama flex py-1 px-3 items-center text-sm text-putih hover:text-utama rounded-full overflow-hidden relative cursor-pointer gap-2 transition-all duration-100 ease-in-out z-[1]  before:inline-block before:translate-y-full before:left-0 before:w-[100%] before:h-[300%] before:absolute before:rounded-full before:z-[-1] before:transition-transform before:ease-in before:duration-300 hover:transition-colors hover:duration-500 hover:ease-in hover:before:-translate-y-0  focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4">
                                <g clip-path="url(#clip0_9_2121)">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.25-7.25a.75.75 0 000-1.5H8.66l2.1-1.95a.75.75 0 10-1.02-1.1l-3.5 3.25a.75.75 0 000 1.1l3.5 3.25a.75.75 0 001.02-1.1l-2.1-1.95h4.59z" clip-rule="evenodd" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_9_2121">
                                        <path d="M0 0h20v20H0z" />
                                    </clipPath>
                                </defs>
                            </svg>
                            <span>Kembali</span>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <div class=" min-h-screen flex justify-center font-jakartasans">
            <div class=" flex items-center">
                <div class=" drop-shadow-3xl bg-putih rounded-xl w-[500px] p-10 flex flex-col items-center">
                    {{-- @foreach ($errors->all() as $item)
                        <small class=" text-merah">{{ $item }}</small>
                    @endforeach --}}
                    <h1 class=" font-medium text-2xl">Daftar</h1>
                    <span class=" text-sm pt-1 text-kedua">Masukkan detail data Anda untuk membuat akun</span>
                    <div class=" w-full pt-8">
                        <form action="{{ route('store.register') }}" method="POST">
                            @csrf
                            <div class=" flex items-center gap-5 mb-5">
                                <div class=" w-1/2">
                                    <label for="username" class=" text-sm text-ketiga">Username</label>
                                    <input id="username" type="text" name="username" placeholder="Username"
                                        class=" text-utama text-sm w-full placeholder:text-sm placeholder:text-ketiga focus:placeholder:text-kedua focus:outline-none mt-2 border border-kedua rounded-md p-3" required>
                                </div>
                                <div class=" w-1/2">
                                    <label for="nama" class=" text-sm text-ketiga">Nama</label>
                                    <input id="nama" type="text" name="nama" placeholder="Name"
                                        class=" text-utama text-sm w-full placeholder:text-sm placeholder:text-ketiga focus:placeholder:text-kedua focus:outline-none mt-2 border border-kedua rounded-md p-3" required>
                                </div>
                            </div>
                            <div class=" flex items-center gap-5 mb-5">
                                <div class=" w-1/2">
                                    <label for="nik" class=" text-sm text-ketiga">Nik</label>
                                    <input id="nik" type="number" name="nik" placeholder="Nik"
                                        class=" text-utama text-sm w-full placeholder:text-sm placeholder:text-ketiga focus:placeholder:text-kedua focus:outline-none mt-2 border border-kedua rounded-md p-3" required>
                                </div>
                                <div class=" w-1/2">
                                    <label for="telp" class=" text-sm text-ketiga">Telp</label>
                                    <input id="telp" type="number" name="telp" placeholder="Telp"
                                        class=" text-utama text-sm w-full placeholder:text-sm placeholder:text-ketiga focus:placeholder:text-kedua focus:outline-none mt-2 border border-kedua rounded-md p-3" required>
                                </div>
                            </div>
                            <div class=" flex items-center gap-5 mb-5">
                                <div class="w-full">
                                    <label for="password"
                                        class=" text-sm text-ketiga duration-300 ease-in-out">Password</label>
                                    <input id="password" name="password" type="password" placeholder="Password"
                                        class=" text-utama text-sm w-full placeholder:text-sm placeholder:text-ketiga focus:placeholder:text-kedua focus:outline-none mt-2 border border-kedua rounded-md p-3" required>
                                </div>
                            </div>
                            <button class=" hover:bg-opacity-80 duration-300 ease-in-out bg-utama w-full text-putih py-3 mb-4 rounded-md">Register</button>
                            <div class=" flex justify-center">
                                <h1 class=" text-sm">Sudah memiliki akun? <a href="{{ route('login') }}"
                                        class=" text-utama hover:underline">Login</a></h1>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
